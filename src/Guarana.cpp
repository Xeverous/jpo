#include "Guarana.hpp"
#include "World.hpp"

Guarana::Guarana(World& world) : Plant(world, 0)
{
}

void Guarana::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	organism_to_buff = &opponent;
	opponent.collision(*this, opponent_position, my_position);
}

void Guarana::draw() const
{
	// TODO
}

void Guarana::die()
{
	if (organism_to_buff != nullptr)
		organism_to_buff->modifyStrength(+3);

	Plant::die();
}

void Guarana::replicate(Vector2d current_position)
{
	world.createNewOrganismFromParents<Guarana>({current_position}, world);
}

const char* Guarana::getTypeName() const
{
	return ::getOrganismName(OrganismType::Guarana);
}

#ifndef PLANT_HPP_
#define PLANT_HPP_
#include "Organism.hpp"

class Plant : public Organism
{
public:
	// all plants have 0 initiative
	Plant(World& world, int strength);

	void action(Vector2d current_position) override;
	// override this to be able to reproduce after rolling seed
	virtual void replicate(Vector2d current_position) = 0;

	static constexpr double replication_chance = 0.3; ///< @brief chance to replicate for all plants - range (0, 1]

	using Organism::collision;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

protected:
	virtual bool rollSeed() const; ///< @brief roll if plant should replicate @return true if so
};

#endif /* PLANT_HPP_ */

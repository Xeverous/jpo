#include "Mosquito.hpp"
#include "World.hpp"
#include <algorithm>

Mosquito::Mosquito(World& world) : Animal(world, 1, 1)
{
}

void Mosquito::collision(Mosquito&, Vector2d my_position, Vector2d opponent_position)
{
	world.createNewOrganismFromParents<Mosquito>({my_position, opponent_position}, world);
}

void Mosquito::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

void Mosquito::die()
{
	std::bernoulli_distribution dist;

	if (dist(::rng))
		Animal::die();
}

int Mosquito::getStrength() const
{
	return strength + countAdjacentMosquitos();
}

int Mosquito::getInitiative() const
{
	return initiative + countAdjacentMosquitos();
}

void Mosquito::draw() const
{
	// TODO
}

const char* Mosquito::getTypeName() const
{
	return ::getOrganismName(OrganismType::Mosquito);
}

unsigned Mosquito::countAdjacentMosquitos() const
{
	Vector2d current_position = world.getPosition(*this);

	std::vector<Vector2d> adjacent_positions;
	world.appendAdjacentPositions(current_position, adjacent_positions);

	return std::count_if(adjacent_positions.begin(), adjacent_positions.end(), [this](Vector2d position)
	{
		const Organism* organism = world.getOrganism(position);

		if (organism == nullptr)
			return false;

		if (organism->isDead())
			return false;

		return dynamic_cast<const Mosquito*>(organism) != nullptr;
	});
}

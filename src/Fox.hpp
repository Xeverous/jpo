#ifndef FOX_HPP_
#define FOX_HPP_
#include "Animal.hpp"

/**
 * @class Fox
 * @details unique behaviour - does not move to fields
 * occupied by stronger organisms
 */
class Fox : public Animal
{
public:
	Fox(World& world);

	void action(Vector2d current_position) override;

	using Animal::collision;
	void collision(Fox& fox, Vector2d my_position, Vector2d opponent_position) override;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

	void draw() const override;
	const char* getTypeName() const override;
};

#endif /* FOX_HPP_ */

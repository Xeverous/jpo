#ifndef GAME_HPP_
#define GAME_HPP_
#include "World.hpp"

class Game
{
public:
	int run();

private:
	World world;
};

#endif /* GAME_HPP_ */

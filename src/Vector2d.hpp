#ifndef VECTOR2D_HPP_
#define VECTOR2D_HPP_
#include <cstddef>

struct Vector2d
{
	std::size_t x;
	std::size_t y;
};

constexpr bool operator<(Vector2d lhs, Vector2d rhs)
{
	if (lhs.x < rhs.x)
		return true;
	else if (lhs.x > rhs.x)
		return false;
	else
		return lhs.y < rhs.y;
}

constexpr bool operator==(Vector2d lhs, Vector2d rhs)
{
	return lhs.x == rhs.x && lhs.y == rhs.y;
}

#endif /* VECTOR2D_HPP_ */

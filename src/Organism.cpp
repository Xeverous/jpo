#include "Organism.hpp"
#include "Animal.hpp"
#include "Sheep.hpp"
#include "Wolf.hpp"
#include "Fox.hpp"
#include "Mosquito.hpp"
#include "Llama.hpp"
#include "Plant.hpp"
#include "Grass.hpp"
#include "Guarana.hpp"
#include "Thorn.hpp"
#include "World.hpp"
#include <stdexcept>

Organism::Organism(World& world, int strength, int initiative)
: world(world), strength(strength), initiative(initiative), age(0)
{
}

int Organism::getStrength() const
{
	return strength;
}

int Organism::getInitiative() const
{
	return initiative;
}

int Organism::getAge() const
{
	return age;
}

void Organism::setStrength(int value)
{
	strength = value;
}

void Organism::setInitiative(int value)
{
	initiative = value;
}

void Organism::modifyStrength(int difference)
{
	strength += difference;
}

void Organism::modifyInitiative(int difference)
{
	initiative += difference;
}

void Organism::setAge(int value)
{
	age = value;
}

void Organism::doAge()
{
	if (isDead())
		throw std::logic_error("dead organisms can't age");

	++age;
}

void Organism::die()
{
	age = dead_age;
}

bool Organism::isDead() const
{
	return age == dead_age;
}

bool Organism::isAlive() const
{
	return !isDead();
}

void Organism::collision(Organism& organism, Vector2d my_position, Vector2d opponent_position)
{
	if (strength >= organism.getStrength())
	{
		organism.die();

		if (organism.isDead()) // some creatures refuse to die
			world.moveOrganism(my_position, opponent_position);
	}
	else
	{
		die();
	}
}

// default cases - each time call more generic case (if not overriden) up to the top Organism type
void Organism::collision(Animal& animal, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Organism&>(animal), my_position, opponent_position);
}

void Organism::collision(Sheep& sheep, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Animal&>(sheep), my_position, opponent_position);
}

void Organism::collision(Wolf& wolf, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Animal&>(wolf), my_position, opponent_position);
}

void Organism::collision(Fox& fox, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Animal&>(fox), my_position, opponent_position);
}

void Organism::collision(Mosquito& mosquito, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Animal&>(mosquito), my_position, opponent_position);
}

void Organism::collision(Llama& llama, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Animal&>(llama), my_position, opponent_position);
}

void Organism::collision(Plant& plant, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Organism&>(plant), my_position, opponent_position);
}

void Organism::collision(Grass& grass, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Plant&>(grass), my_position, opponent_position);
}

void Organism::collision(Guarana& guarana, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Plant&>(guarana), my_position, opponent_position);
}

void Organism::collision(Thorn& thorn, Vector2d my_position, Vector2d opponent_position)
{
	collision(static_cast<Plant&>(thorn), my_position, opponent_position);
}

// visitor
void Organism::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

void Organism::writeBaseStats(std::ostream& os) const
{
	os << strength << data_io::value_delimeter << initiative << data_io::value_delimeter << age << data_io::value_delimeter;
}

void Organism::save(std::ostream& os) const
{
	os << getTypeName() << data_io::value_delimeter;
	writeBaseStats(os);
	os << '\n';
}

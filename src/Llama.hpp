#ifndef LLAMA_HPP_
#define LLAMA_HPP_
#include "Animal.hpp"

/**
 * @class Llama
 * @details unique behaviour - increases it's strength through eating plants
 */
class Llama : public Animal
{
public:
	Llama(World& world);

	using Animal::collision;
	void collision(Llama& llama, Vector2d my_position, Vector2d opponent_position) override; // reproduction
	void collision(Plant& plant, Vector2d my_position, Vector2d opponent_position) override; // eating
	void collision(Thorn& thorn, Vector2d my_position, Vector2d opponent_position) override; // override back to parent class - thorns won't be eaten

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

	void draw() const override;
	const char* getTypeName() const override;

private:
	int eaten_plants = 0;
};

#endif /* LLAMA_HPP_ */

#include "Game.hpp"
#include <memory>
#include <random>
#include <ctime>

std::default_random_engine rng(std::time(nullptr));

int main()
{
	auto game = std::make_unique<Game>();
	return game->run();
}

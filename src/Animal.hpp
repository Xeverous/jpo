#ifndef ANIMAL_HPP_
#define ANIMAL_HPP_
#include "Organism.hpp"

class Animal : public Organism
{
public:
	Animal(World& world, int strength, int initiative);

	/**
	 * @brief default action behaviour - move to adjacent field
	 * @details if encountered different specie - fight
	 * if encountered same specie - repreduce
	 */
	void action(Vector2d my_position) override;

	using Organism::collision;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;
};

#endif /* ANIMAL_HPP_ */

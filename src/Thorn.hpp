#ifndef THORN_HPP_
#define THORN_HPP_
#include "Plant.hpp"

/**
 * @class Thorn
 * @details unique behaviour - has little strength and always successfully replicates
 */
class Thorn : public Plant
{
public:
	Thorn(World& world);

	using Plant::collision;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

	void replicate(Vector2d current_position) override;
	bool rollSeed() const override;

	void draw() const override;
	const char* getTypeName() const override;
};

#endif /* THORN_HPP_ */

#ifndef GRASS_HPP_
#define GRASS_HPP_
#include "Plant.hpp"

class Grass : public Plant
{
public:
	Grass(World& world);

	using Plant::collision;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

	void replicate(Vector2d current_position) override;

	void draw() const override;
	const char* getTypeName() const override;
};

#endif /* GRASS_HPP_ */

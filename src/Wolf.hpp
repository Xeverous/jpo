#ifndef WOLF_HPP_
#define WOLF_HPP_
#include "Animal.hpp"

class Wolf : public Animal
{
public:
	Wolf(World& world);

	using Animal::collision;
	void collision(Wolf& wolf, Vector2d my_position, Vector2d opponent_position) override;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

	void draw() const override;
	const char* getTypeName() const override;
};

#endif /* WOLF_HPP_ */

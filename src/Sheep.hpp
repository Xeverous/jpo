#ifndef SHEEP_HPP_
#define SHEEP_HPP_
#include "Animal.hpp"

class Sheep : public Animal
{
public:
	Sheep(World& world);

	using Animal::collision;
	void collision(Sheep& sheep, Vector2d my_position, Vector2d opponent_position) override;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

	void draw() const override;
	const char* getTypeName() const override;
};

#endif /* SHEEP_HPP_ */

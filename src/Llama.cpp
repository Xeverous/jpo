#include "Llama.hpp"
#include "Plant.hpp"
#include "Thorn.hpp"
#include "World.hpp"

Llama::Llama(World& world) : Animal(world, 4, 3)
{
}

void Llama::collision(Llama&, Vector2d my_position, Vector2d opponent_position)
{
	world.createNewOrganismFromParents<Llama>({my_position, opponent_position}, world);
}

void Llama::collision(Plant& plant, Vector2d my_position, Vector2d opponent_position)
{
	Animal::collision(plant, my_position, opponent_position);

	if (isAlive() and plant.isDead())
		++strength;
}

void Llama::collision(Thorn& thorn, Vector2d my_position, Vector2d opponent_position)
{
	Animal::collision(thorn, my_position, opponent_position);
}

void Llama::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

void Llama::draw() const
{
	// TODO
}

const char* Llama::getTypeName() const
{
	return ::getOrganismName(OrganismType::Llama);
}

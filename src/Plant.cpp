#include "Plant.hpp"
#include "Utility.hpp"

Plant::Plant(World& world, int strength)
: Organism(world, strength, 0)
{
}

void Plant::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

void Plant::action(Vector2d current_position)
{
	if (rollSeed())
		replicate(current_position);
}

bool Plant::rollSeed() const
{
	std::bernoulli_distribution dist(replication_chance);
	return dist(::rng);
}

#include "Animal.hpp"
#include "World.hpp"

Animal::Animal(World& world, int strength, int initiative)
: Organism(world, strength, initiative)
{
}

void Animal::action(Vector2d my_position)
{
	Vector2d new_position = world.generateAdjacentPosition(my_position);

	if (world.isFree(new_position))
		world.moveOrganism(my_position, new_position);
	else
		world.getOrganism(new_position)->collideWith(*this, my_position, new_position);
}

void Animal::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

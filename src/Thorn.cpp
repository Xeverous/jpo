#include "Thorn.hpp"
#include "World.hpp"

Thorn::Thorn(World& world) : Plant(world, 2)
{
}

void Thorn::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

void Thorn::draw() const
{
	// TODO
}

void Thorn::replicate(Vector2d current_position)
{
	world.createNewOrganismFromParents<Thorn>({current_position}, world);
}

bool Thorn::rollSeed() const
{
	return true;
}

const char* Thorn::getTypeName() const
{
	return ::getOrganismName(OrganismType::Thorn);
}

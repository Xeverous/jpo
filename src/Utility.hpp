#ifndef UTILITY_HPP_
#define UTILITY_HPP_
#include <random>
#include <array>

extern std::default_random_engine rng;

enum class OrganismType
{
	Wolf,
	Sheep,
	Fox,
	Mosquito,
	Llama,
	Grass,
	Guarana,
	Thorn,

	MAX
};

constexpr std::array<const char* const, static_cast<std::size_t>(OrganismType::MAX)> organism_type_names =
{
	"Wolf",
	"Sheep",
	"Fox",
	"Mosquito",
	"Llama",
	"Grass",
	"Guarana",
	"Thorn"
};

constexpr const char* getOrganismName(OrganismType organism_type)
{
	return organism_type_names[static_cast<std::size_t>(organism_type)];
}

namespace data_io
{
	constexpr const char* empty_field_token = "@";
	constexpr       char  value_delimeter   = ';';
	constexpr const char* default_save_path = "save.csv";
}

#endif /* UTILITY_HPP_ */

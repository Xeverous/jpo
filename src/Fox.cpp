#include "Fox.hpp"
#include "World.hpp"

Fox::Fox(World& world) : Animal(world, 3, 7)
{
}

void Fox::collision(Fox&, Vector2d my_position, Vector2d opponent_position)
{
	world.createNewOrganismFromParents<Fox>({my_position, opponent_position}, world);
}

void Fox::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

void Fox::draw() const
{
	// TODO
}

void Fox::action(Vector2d current_position)
{
	Vector2d new_position = world.generateAdjacentPosition(current_position);

	if (world.isFree(new_position))
	{
		world.moveOrganism(current_position, new_position);
	}
	else
	{
		// fox does not attack stronger organisms
		Organism& opponent = *world.getOrganism(new_position);

		if (opponent.getStrength() <= strength)
			opponent.collideWith(*this, current_position, new_position);
	}
}

const char* Fox::getTypeName() const
{
	return ::getOrganismName(OrganismType::Fox);
}

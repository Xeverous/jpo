#ifndef MOSQUITO_HPP_
#define MOSQUITO_HPP_
#include "Animal.hpp"

/**
 * @class Mosquito
 * @details unique behaviour - refuses to die, stronger when adjacent to other mosquitos
 */
class Mosquito : public Animal
{
public:
	Mosquito(World& world);

	using Animal::collision;
	void collision(Mosquito& mosquito, Vector2d my_position, Vector2d opponent_position) override;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

	void die() override;
	int getStrength() const override;
	int getInitiative() const override;

	void draw() const override;
	const char* getTypeName() const override;
private:
	unsigned countAdjacentMosquitos() const;
};

#endif /* MOSQUITO_HPP_ */

#include "Grass.hpp"
#include "Utility.hpp"
#include "World.hpp"

Grass::Grass(World& world)
: Plant(world, 0)
{
}

void Grass::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

void Grass::draw() const
{
	// TODO
}

void Grass::replicate(Vector2d current_position)
{
	world.createNewOrganismFromParents<Grass>({current_position}, world);
}

const char* Grass::getTypeName() const
{
	return ::getOrganismName(::OrganismType::Grass);
}

#ifndef ORGANISM_HPP_
#define ORGANISM_HPP_
#include "Vector2d.hpp"
#include <fstream>

class World;

class Animal;
class Sheep;
class Wolf;
class Fox;
class Mosquito;
class Llama;

class Plant;
class Grass;
class Guarana;
class Thorn;

class Organism
{
public:
	Organism(World& world, int strength, int initiative);
	virtual ~Organism() = default;

	Organism(const Organism& other) = delete;
	Organism& operator=(const Organism& other) = delete;

	virtual int getStrength() const;
	virtual int getInitiative() const;
	int getAge() const;

	// might later make these virtual if needed
	void setStrength(int strength);
	void setInitiative(int initiative);
	void modifyStrength(int difference);   // for buffs and debuffs
	void modifyInitiative(int difference); // for buffs and debuffs
	void setAge(int age);

	void doAge();
	virtual void die();
	bool isDead() const;
	bool isAlive() const; // to avoid double negatives in if (!isDead())

	static constexpr int dead_age = -1;

	virtual void action(Vector2d current_position) = 0;

	/**
	 * @defgroup actual colision functions for each type of organism
	 *
	 * by default all call base class case (unless overriden)
	 *
	 * Note 1: templates can not be virtual so there is explicitly written overload for each type
	 * Note 2: place using BaseClass::collision; in each derived class to inform overload resolution to search also in base classes,
	 * otherwise these functions will be name-shadowed and not participate in the list of candidates to call
	 *
	 * @{
	 *
	 * @brief default collision behaviour - fight: kill or be killed
	 */
	virtual void collision(Organism& organism, Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Animal& animal,     Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Sheep& sheep,       Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Wolf& wolf,         Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Fox& fox,           Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Mosquito& mosquito, Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Llama& llama,       Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Plant& plant,       Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Grass& grass,       Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Guarana& guarana,   Vector2d my_position, Vector2d opponent_position);
	virtual void collision(Thorn& thorn,       Vector2d my_position, Vector2d opponent_position);

	/** @} */

	/**
	 * @brief visitor function dispatching callers to their actual types
	 * @details each derived class must overide this to be able to be dispatched to it's actual type
	 * in each derived class implementation simply write
	 * opponent.collision(*this, opponent_position, my_position);
	 * an appropriate overload of collision() will be selected because type of *this is always known exactly
	 */
	virtual void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position);

	virtual void draw() const = 0;
	virtual const char* getTypeName() const = 0;
	virtual void save(std::ostream& os) const;

protected:
	void writeBaseStats(std::ostream& os) const;

	World& world;
	int strength;
	int initiative;
	int age;
};

#endif /* ORGANISM_HPP_ */

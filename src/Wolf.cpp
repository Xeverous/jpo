#include "Wolf.hpp"
#include "World.hpp"

Wolf::Wolf(World& world)
: Animal(world, 9, 5)
{
}

void Wolf::collision(Wolf&, Vector2d my_position, Vector2d opponent_position)
{
	world.createNewOrganismFromParents<Wolf>({my_position, opponent_position}, world);
}

void Wolf::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

void Wolf::draw() const
{
	// TODO
}

const char* Wolf::getTypeName() const
{
	return ::getOrganismName(::OrganismType::Wolf);
}

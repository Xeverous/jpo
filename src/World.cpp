#include "World.hpp"
#include "Utility.hpp"
#include "Wolf.hpp"
#include "Sheep.hpp"
#include "Fox.hpp"
#include "Mosquito.hpp"
#include "Llama.hpp"
#include "Grass.hpp"
#include "Guarana.hpp"
#include "Thorn.hpp"
#include <algorithm>
#include <utility>
#include <ctime>
#include <type_traits>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <functional>

void World::draw() const
{
	// TODO
}

void World::processTurn()
{
	++turn_count;

	// for sorting objects by initiative
	std::vector<std::reference_wrapper<Organism>> vec;

	// extract existing entities to sort them by initiative
	// this reduces turn complexity from O(n^2) to O(n + n log n)
	for (std::size_t y = 0; y < board.size(); ++y)
		for (std::size_t x = 0; x < board[y].size(); ++x)
		{
			Organism* ptr = board[y][x].get();

			if (ptr != nullptr)
			{
				vec.emplace_back(*ptr);
				map[ptr] = {x, y};
			}
		}

	std::sort(vec.begin(), vec.end(), [](const Organism& lhs, const Organism& rhs)
	{
		// this can be simplified with C++20 operator <=>
		if (lhs.getInitiative() == rhs.getInitiative())
			return lhs.getAge() > rhs.getAge();
		else
			return lhs.getInitiative() > rhs.getInitiative();
	});

	for (Organism& organism : vec)
		organism.doAge();

	for (Organism& organism : vec)
		if (organism.isAlive()) // some things can die before their's turn
			organism.action(map[&organism]);

	// remove dead organisms
	for (auto& row : board)
		for (auto& organism_ptr : row)
			if (organism_ptr != nullptr && organism_ptr->isDead())
				organism_ptr.reset();

	map.clear();
}

void World::moveOrganism(Vector2d old_position, Vector2d new_position)
{
	auto& current_org = board[old_position.y][old_position.x];
	auto& target_org  = board[new_position.y][new_position.x]; // may be null (empty tile on the board)

	map[current_org.get()] = new_position;

	if (target_org != nullptr)
		map[target_org.get()] = old_position;

	std::swap(current_org, target_org);
}

bool World::isValid(Vector2d position) const
{
	return position.x < getBoardSize().x and position.y < getBoardSize().y;
}

bool World::isFree(Vector2d position) const
{
	const auto& organism_ptr = board[position.y][position.x];

	if (organism_ptr == nullptr)
		return true;
	else
		return organism_ptr->isDead();
}

Organism* World::getOrganism(Vector2d position)
{
	return board[position.y][position.x].get();
}

const Organism* World::getOrganism(Vector2d position) const
{
	return board[position.y][position.x].get();
}

Vector2d World::getPosition(const Organism& organism) const
{
	return map.find(&organism)->second;
}

Vector2d World::getBoardSize() const
{
	return { board.front().size(), board.size() };
}

Vector2d World::generateAdjacentPosition(Vector2d position) const
{
	enum Direction { up, down, left, right };
	std::uniform_int_distribution<std::underlying_type_t<Direction>> dist(0, right);

	auto generate = [&]() -> Vector2d
	{
		auto direction = static_cast<Direction>(dist(::rng));

		if (direction == up)
			return { position.x,     position.y - 1 };
		else if (direction == down)
			return { position.x,     position.y + 1 };
		else if (direction == left)
			return { position.x - 1, position.y     };
		else // if (direction == right)
			return { position.x + 1, position.y     };
	};

	Vector2d result;

	do
	{
		result = generate();
	} while(!isValid(result));

	return result;
}

Vector2d World::generateRandomFreePosition() const
{
	std::vector<Vector2d> empty_positions;

	for (std::size_t y = 0; y < board.size(); ++y)
		for (std::size_t x = 0; x < board[y].size(); ++x)
		{
			Vector2d pos{x, y};

			if (isFree(pos))
				empty_positions.push_back(pos);
		}

	if (empty_positions.empty())
		throw std::logic_error("no empty positions on the board");

	std::uniform_int_distribution<std::size_t> dist(0, empty_positions.size() - 1);
	return empty_positions[dist(::rng)];
}

void World::save() const
{
	std::ofstream file(data_io::default_save_path, std::ios_base::binary | std::ios_base::trunc);

	if (!file.good())
		throw std::runtime_error("can't open or create file for save");

	for (const auto& row : board)
		for (const auto& organism : row)
			if (organism == nullptr)
				file << data_io::empty_field_token << data_io::value_delimeter <<'\n';
			else
				organism->save(file);

	file << turn_count << data_io::value_delimeter << '\n';
}

void World::load()
{
	// use boost::filesystem or std::filesystem from C++17 to access all *.csv in the current directory
	std::ifstream file(data_io::default_save_path);

	if (!file.good())
		throw std::runtime_error("can't open save file");

	std::string line;
	std::string token;
	std::istringstream ss;
	std::size_t i = 0;
	const std::size_t fields = getBoardSize().x * getBoardSize().y;

	auto loadNextToken = [&]() // maybe use std::regex?
	{
		if (!std::getline(ss, token, data_io::value_delimeter))
			throw std::runtime_error("parse error: invalid line " + std::to_string(i + 1) + " with contents:\n" + line);
	};

	auto getCurrentField = [&]() -> std::unique_ptr<Organism>&
	{
		return board[i / getBoardSize().x][i % getBoardSize().x];
	};

	for (; std::getline(file, line); ++i)
	{
		if (i == fields) // exit when loaded everything (in case file contains too many lines)
			break;

		ss.rdbuf()->pubsetbuf(&line.front(), line.size()); // avoid copying by setting buffer from string

		loadNextToken();

		if (token == data_io::empty_field_token)
		{
			getCurrentField().reset();
			continue;
		}

		std::string organism_type = std::move(token);
		loadNextToken();
		int strength = std::stoi(token);
		loadNextToken();
		int initiative = std::stoi(token);
		loadNextToken();
		int age = std::stoi(token);
		bool valid_organism_type = false;

		for (std::size_t j = 0; j < static_cast<std::size_t>(OrganismType::MAX); ++j)
			if (organism_type == ::getOrganismName(static_cast<OrganismType>(j)))
			{
				createNewOrganism({i % getBoardSize().x, i / getBoardSize().x}, static_cast<OrganismType>(j));
				valid_organism_type = true;
				break;
			}

		if (valid_organism_type)
		{
			getCurrentField()->setStrength(strength);
			getCurrentField()->setInitiative(initiative);
			getCurrentField()->setAge(age);
		}
		else
		{
			throw std::runtime_error("parsing error: invalid organism type: " + organism_type);
		}
	}

	if (i < fields)
		throw std::runtime_error("parsing error: save file too short - only " + std::to_string(i) + " lines out of " + std::to_string(fields));

	turn_count = std::stoi(line);
}

void World::appendAdjacentPositions(Vector2d origin, std::vector<Vector2d>& positions) const
{
	auto attempt = [&, this](Vector2d position)
	{
		if (isValid(position))
			positions.push_back(position);
	};

	attempt({origin.x - 1, origin.y    });
	attempt({origin.x + 1, origin.y    });
	attempt({origin.x    , origin.y - 1});
	attempt({origin.x    , origin.y + 1});
}

void World::generateRandomWorld()
{
	std::vector<Vector2d> all_positions;
	all_positions.reserve(getBoardSize().x * getBoardSize().y);

	for (std::size_t y = 0; y < board.size(); ++y)
		for (std::size_t x = 0; x < board[y].size(); ++x)
		{
			all_positions.push_back({x, y});
			board[y][x].reset();
		}

	std::vector<OrganismType> organisms_to_create;

	// add arbitrary number of each organism
	for (std::size_t i = 0; i < static_cast<std::size_t>(OrganismType::MAX); ++i)
		for (int j = 0; j < 5; ++j)
			organisms_to_create.push_back(static_cast<OrganismType>(i));

	if (all_positions.size() < organisms_to_create.size())
		throw std::logic_error("too many organisms to create");

	// shuffle positions so that organisms are created at random places
	std::shuffle(all_positions.begin(), all_positions.end(), ::rng);

	for (std::size_t i = 0; i < organisms_to_create.size(); ++i)
		createNewOrganism(all_positions[i], organisms_to_create[i]);
}

void World::createNewOrganism(Vector2d position, OrganismType organism_type)
{
	if (organism_type == OrganismType::Wolf)
		createNewOrganism<Wolf>(position, *this);
	else if (organism_type == OrganismType::Sheep)
		createNewOrganism<Sheep>(position, *this);
	else if (organism_type == OrganismType::Fox)
		createNewOrganism<Fox>(position, *this);
	else if (organism_type == OrganismType::Mosquito)
		createNewOrganism<Mosquito>(position, *this);
	else if (organism_type == OrganismType::Llama)
		createNewOrganism<Llama>(position, *this);
	else if (organism_type == OrganismType::Grass)
		createNewOrganism<Grass>(position, *this);
	else if (organism_type == OrganismType::Guarana)
		createNewOrganism<Guarana>(position, *this);
	else // if (organism_type == OrganismType::Thorn)
		createNewOrganism<Thorn>(position, *this);
}

void World::removeDuplicatePositions(std::vector<Vector2d>& positions)
{
	std::sort(positions.begin(), positions.end());

	positions.erase(
		std::unique(positions.begin(), positions.end()),
		positions.end());
}

void World::removeOccupiedPositions(std::vector<Vector2d>& positions) const
{
	positions.erase(
		std::remove_if(positions.begin(), positions.end(), [this](Vector2d position){ return !isFree(position); }),
		positions.end());
}

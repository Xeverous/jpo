#ifndef WORLD_HPP_
#define WORLD_HPP_
#include "Organism.hpp"
#include "Vector2d.hpp"
#include "Utility.hpp"
#include <unordered_map>
#include <memory>
#include <vector>
#include <utility>
#include <random>
#include <algorithm>
#include <initializer_list>

class World
{
public:
	World() = default;
	World(const World& other) = delete;
	World& operator=(const World& other) = delete;
	World(World&& other) = delete;
	World& operator=(World&& other) = delete;

	void draw() const;
	void processTurn();
	/**
	 * @brief moves organism from one position to another
	 * @details if both positions are occupied, organisms are swapped
	 * @param old_position current position of the organism, must be occupied
	 * @param new_position position to move to
	 */
	void moveOrganism(Vector2d old_position, Vector2d new_position);

	bool isValid(Vector2d position) const;
	bool isFree(Vector2d position) const;
	Vector2d getBoardSize() const;
	Vector2d generateAdjacentPosition(Vector2d position) const;
	Vector2d generateRandomFreePosition() const;
	const Organism* getOrganism(Vector2d position) const; ///< @brief may return null
	Organism* getOrganism(Vector2d position);             ///< @brief may return null
	Vector2d getPosition(const Organism& organism) const;

	/**
	 * @brief Adds adjacent positions based on the given origin
	 * @details Function adds up to 4 (only valid) positions
	 * @param origin position to which add/subtract 1 on x/y axis
	 * @param positions vector to fill with results
	 */
	void appendAdjacentPositions(Vector2d origin, std::vector<Vector2d>& positions) const;

	static void removeDuplicatePositions(std::vector<Vector2d>& positions);
	void removeOccupiedPositions(std::vector<Vector2d>& positions) const;

	/**
	 * @brief creates new organism adjacent to one of the parents
	 * @details may fail if there is no space
	 * @tparam T type of new organism to create
	 * @tparam Args types of arguments
	 * @param parent_positions positions of each parent
	 * @param args parameter pack of arguments to construct organism (for custom constructors)
	 * @return true if creation succeeded
	 */
	template<typename Organism, typename... Args>
	bool createNewOrganismFromParents(std::initializer_list<Vector2d> parents_positions, Args&&... args);

	void save() const;
	void load();

	void generateRandomWorld();

private:

	/**
	 * @brief spawns a new organism with custom arguments
	 */
	template <typename Organism, typename... Args>
	void createNewOrganism(Vector2d position, Args&&... args);
	/**
	 * @brief spawns a new organism with default Organism ctor
	 */
	void createNewOrganism(Vector2d position, OrganismType organism_type);

	int turn_count = 0;

	/*
	 * reduce search complexity by providing 2 data structures - for searching in both directions
	 * some organisms will ask for adjacent position neighbours, some will ask for their own position
	 * a more correct (and probably robust) implementation would use BSP-tree, KD-tree, Octree or boost::bimap
	 */
	// hash map for fast object => position search
	std::unordered_map<const Organism*, Vector2d> map;
	// 2D array for fast position => object search
	std::array<std::array<std::unique_ptr<Organism>, 20>, 20> board;
};

template<typename Organism, typename... Args>
bool World::createNewOrganismFromParents(std::initializer_list<Vector2d> parents_positions, Args&&... args)
{
	std::vector<Vector2d> valid_positions;

	for (Vector2d pos : parents_positions)
		appendAdjacentPositions(pos, valid_positions);

	removeDuplicatePositions(valid_positions);
	removeOccupiedPositions(valid_positions);

	if (valid_positions.empty())
		return false;

	std::uniform_int_distribution<std::size_t> dist(0, valid_positions.size() - 1);
	Vector2d creation_position = valid_positions[dist(::rng)];
	createNewOrganism<Organism>(creation_position, std::forward<Args>(args)...);
	return true;
}

template<typename Org, typename... Args>
void World::createNewOrganism(Vector2d position, Args&&... args)
{
	static_assert(std::is_base_of<Organism, Org>::value, "Type to create must be inherited from Organism");
	// update both map and array
	auto& ptr = board[position.y][position.x];
	ptr = std::make_unique<Org>(std::forward<Args>(args)...); // perfect forwarding idiom
	map.insert({ptr.get(), position});
}

#endif /* WORLD_HPP_ */

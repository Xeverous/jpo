#ifndef GUARANA_HPP_
#define GUARANA_HPP_
#include "Plant.hpp"

/**
 * @class Guarana
 * @details unique behaviour - add 3 to strength to animal who eats it (when guarana dies)
 */
class Guarana : public Plant
{
public:
	Guarana(World& world);

	using Plant::collision;

	void collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position) override;

	void replicate(Vector2d current_position) override;
	void die() override;

	void draw() const override;
	const char* getTypeName() const override;
private:
	Organism* organism_to_buff = nullptr;
};

#endif /* GUARANA_HPP_ */

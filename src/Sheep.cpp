#include "Sheep.hpp"
#include "World.hpp"

Sheep::Sheep(World& world)
: Animal(world, 4, 4)
{
}

void Sheep::draw() const
{
	// TODO
}

void Sheep::collision(Sheep&, Vector2d my_position, Vector2d opponent_position)
{
	world.createNewOrganismFromParents<Sheep>({my_position, opponent_position}, world);
}

void Sheep::collideWith(Organism& opponent, Vector2d opponent_position, Vector2d my_position)
{
	opponent.collision(*this, opponent_position, my_position);
}

const char* Sheep::getTypeName() const
{
	return ::getOrganismName(::OrganismType::Sheep);
}

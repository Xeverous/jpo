# Virtual World Simulator

Example 2D world simulation with multiple animal and plant polymorphic types

**Switch branch to console drawing. The code on master branch has empty drawing functions.**

Rules

- TODO


Recommended source reading order

- `main.cpp`
- Utility, Vector2d
- Organism
- Plant, Animal
- rest

Implementation - modern C++14

- no raw, owning pointers
- not a single `new`/`delete` expression
- const correctness, `constexpr`
- no code duplication
- lots of `auto`

Implemented features

- core turn logic
- collision (through visitor pattern)
- organism replication
- save/load world state from file
- console drawing (on branch)
- logging (on branch)

TODO features

- GUI
